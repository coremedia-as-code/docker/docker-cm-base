#!/bin/bash

set -e

HADOLINT_VERSION='2.8.0'
HADOLINT_PATH="/tmp/hadolint_${HADOLINT_VERSION}"

if ! [ -e ${HADOLINT_PATH} ]
then
  curl \
    --silent \
    --location \
    --output "${HADOLINT_PATH}" \
    "https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64"

  chmod +x "${HADOLINT_PATH}"
fi

${HADOLINT_PATH} Dockerfile
#shellcheck rootfs/init/*.sh \
#  --shell=sh \
#  --external-sources \
#  --exclude=SC1091,SC2181,SC2046

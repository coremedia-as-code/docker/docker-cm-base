# ---------------------------------------------------------------------------------------
#
# downloads

ARG BASE_IMAGE_TAG

FROM alpine:3 as stage1
ARG PROMETHEUS_JAVAAGENT=0.16.1
ARG COMPOSE_WAIT=2.9.0

# hadolint ignore=DL3017,DL3018,DL3019
RUN \
  apk update  --quiet

# hadolint ignore=DL3018,DL3019
RUN \
  apk add --quiet   \
    curl

WORKDIR /tmp/

RUN \
  curl \
    --silent \
    --location \
    --retry 3 \
    --output confd \
    https://github.com/kelseyhightower/confd/releases/download/v0.16.0/confd-0.16.0-linux-amd64

RUN \
  curl \
    --silent \
    --location \
    --retry 3 \
    --output jmx_prometheus_javaagent.jar \
    https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/${PROMETHEUS_JAVAAGENT}/jmx_prometheus_javaagent-${PROMETHEUS_JAVAAGENT}.jar

RUN \
  curl \
    --silent \
    --location \
    --retry 3 \
    --output docker-compose-wait \
    https://github.com/ufoscout/docker-compose-wait/releases/download/${COMPOSE_WAIT}/wait

RUN \
  chmod +x confd docker-compose-wait

# ---------------------------------------------------------------------------------------

FROM openjdk:${BASE_IMAGE_TAG}
ARG MAINTAINER='CoreMedia AG <support@coremedia.com>'
ARG BUILD_DATE
ARG GIT_BUILD_REF="unknown"

LABEL \
  build-date="${BUILD_DATE}" \
  GIT_BUILD_REF=${GIT_BUILD_REF} \
  maintainer="${MAINTAINER}" \
  org.label-schema.build-date=${BUILD_DATE}

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV SPRING_PROFILES=default \
    PROMETHEUS=true \
    DEBUG_ENTRYPOINT=false \
    JAVA_OPTS="-Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses" \
    JAVA_DEBUG=false \
    JAVA_DEBUG_SUSPEND=false \
    EXIT_ON_OOM=true \
    HEAP_DUMP_ON_OOM=false \
    HEAP_DUMP_FILENAME="" \
    LANG=C.UTF-8 \
    # set this to either env or file
    CONFD_BACKEND="env" \
    # prefix for keys i.e. dev/management dev/master dev/repl or uat/managment etc.
    # a prefix dev/management for example requires environment variables prefixed with DEV_MANAGEMENT_
    CONFD_PREFIX="" \
    SPRING_BOOT_EXPLODED=false \
    WAIT_LOGGER_LEVEL=info \
    # abort threshold in seconds
    WAIT_TIMEOUT=180 \
    # interval between probes in seconds
    WAIT_SLEEP_INTERVAL=10

# Packages required for:
# * curl          - used for healthchecks and entrypoint wait scripts.
# * coreutils     - required for some calls from CoreMedias common.sh
# * zip & unzip   - both requires for several tooling steps
#
# Directories:
# * /coremedia/log                - if there are more than one log file, we can create a volume for this dir and
#                                   use a second container in the pod to forward the log to stdout
# * /coremedia/heapdumps          - if HEAP_DUMP_ON_OOM is active dumps will be placed in this directory either using
#                                   HEAP_DUMP_FILENAME as a file name or a current formatted timestamp, both with added
#                                   .dump as file extension, i.e. /coremedia/heapdumps/20190802_143146.hprof
# * /coremedia/tools/var/logs     - as long as tooling has its own logging that is not configurable like spring-boot logging,
#                                   we need to create these directories in advance. We do it in the base image, so we
#                                   don't repeat this step in many Dockerfiles.
# * /coremedia/corem-home/var/tmp - temporary directory we still need for backwards compatibility. It should not be
#                                   required but it does not hurt. No data will be written to it.
# hadolint ignore=DL3027
RUN apt update && \
    apt \
      --assume-yes \
        dist-upgrade && \
    apt \
      --assume-yes \
      --no-install-recommends \
        install \
          ca-certificates \
          curl \
          coreutils \
          zip \
          unzip && \
    apt \
      --assume-yes \
        clean && \
    apt \
      --assume-yes \
        autoremove  && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    groupadd -g 1000 coremedia && \
    useradd -u 1000 -d /coremedia -g coremedia coremedia && \
    mkdir -p /coremedia/cache \
             /coremedia/tools/var/logs \
             /coremedia/corem-home/var/tmp \
             /coremedia/log \
             /coremedia/heapdumps \
             /etc/confd/templates \
             /etc/confd/conf.d && \
    chown -R coremedia:coremedia /coremedia /etc/confd

COPY \
  --chown=coremedia:coremedia \
  --from=stage1 \
    /tmp/jmx_prometheus_javaagent.jar \
    /coremedia/prometheus/
COPY \
  --from=stage1 \
    /tmp/confd \
    /usr/bin/
COPY \
  --from=stage1 \
    /tmp/docker-compose-wait \
    /usr/bin/
COPY \
  --chown=coremedia:coremedia \
    src/docker \
    /coremedia
# copy as root
COPY \
  src/docker-java/default.policy \
  /usr/local/openjdk-11/lib/security/default.policy

USER coremedia:coremedia

WORKDIR /coremedia
ENTRYPOINT ["./entrypoint.sh"]

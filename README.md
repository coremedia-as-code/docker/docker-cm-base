This image provides the infrastructure to start Spring-Boot based applications. Depending on the tag, this image is
based on:
- `<VERSION>-corretto-11` - [Amazon Corretto](https://hub.docker.com/_/amazoncorretto)
- `<VERSION>-openjdk-11-(jre|jdk)` - [OpenJdk](https://hub.docker.com/_/openjdk)

Specification
=============

## Layout
```
/coremedia
        |- entrypoint.sh
        |- application
        |- confd
        |- wait-for-content-management-server (deprecated)
        |- wait-for-master-live-server (deprecated)
        |- wait-for-replication-live-server (deprecated)
        |- wait-for-workflow-server (deprecated)
        |- wait-for-file (deprecated)
        |- prometheus
        |   `- jmx-prometheus.yml
        `- tools
            |- bin
            |   |- groovysh.jpif
            |   `- pre-config.jpif
            `- properties
                `-corem
                   `- tools-logback.xml
```

Working Directory: `/coremedia`

User: `coremedia`, please make sure that you copy/add files in your Dockerfile using the `--chown=coremedia:coremedia` directive.

The files below `/coremedia/tools` are required to support CoreMedia `cm` based tools.

## Entrypoint scripts

This image provides an entrypoint infrastructure to chain scripts in advance of the application start. The chain is started using the `entrypoint.sh` script and ended with
the `application` script. Each script except for the `application` script passes the remaining script chain as arguments to the next script using `exec $@`. For debugging
purpose each script should contain the following snippet at its top to enable shell debugging when the environment variable `DEBUG_ENTRYPOINT` is set to `true`.

```
if [ "${DEBUG_ENTRYPOINT}" = "true" ]; then
  echo "[DOCKER ENTRYPOINT] - DEBUG_ENTRYPOINT detected, all commands will be printed"
  set -x
fi
```

The chain can be configured using the `CMD` array in your `Dockerfile` based on this image.

The entrypoint script includes the wait command provided by [docker-compose-wait](https://github.com/ufoscout/docker-compose-wait) . To wait for
services dependencies, simply use the environment variables for configuration.

### `application`

The application script starts the Spring-Boot application, it has to be the last command in the chain.

* `SPRING_PROFILES` - comma separated list of Spring profiles to activate.
* `PROMETHEUS` - boolean flag to enable Prometheus agent. The configuration for the agent will be loaded from `/coremedia/prometheus/jmx-prometheus.yml`. If
  set to true, the Prometheus agent will expose his metrics at port `8199`.
* `JAVA_OPTS` - Java runtime options. For defaults see Dockerfile.
* `JAVA_DEBUG` - boolean flag to enable debug mode.
* `JAVA_DEBUG_SUSPEND` - boolean flag to suspend startup until debugger is attached.
  environment variable.
* `APPLICATION_OPTS` - arbitrary application opts to pass to the java execution.
* `JAVA_HEAP` - the amaout of heap memory, i.e. `512m` or `2g`
* `EXIT_ON_OOM` - exit on case of an out of memory error. Defaults to true.
* `HEAP_DUMP_ON_OOM` - write a heap dump to `/coremedia/heapdumps/` in case of an out of memory error.
* `JAVA_Z_GC` - enable the new Z garbage collector. This is experimental in Java 11.
* `JAVA_Z_GC_THREADS` - the number of concurrent threads to use. Default is empty, which means that the JVM calculates a value.
* `JAVA_PARALLEL_GC` - enable the parallel garbage collector.
* `JAVA_PARALLEL_GC_THREADS` - the number of concurrent threads to use. Default is empty, which means that the JVM calculates a value.

* `DEVELOPMENT_SETUP` - prepends the spring profiles list with `dev`.

### `wait-for-*`

The `wait-for-*` scripts are deprecated, please use the environment variables described in [docker-compose-wait](https://github.com/ufoscout/docker-compose-wait) , i.e.

```
WAIT_HOSTS="content-management-server:8083,workflow-server:8083"
WAIT_PATHS="/some/marker/file,/some/other/marker/file"
```

### `confd`

This entrypoint runs [confd](https://github.com/kelseyhightower/confd/tree/v0.16.0), a configuration tool that renders
[go templates](https://golang.org/pkg/text/template/) with
[confd template functions](https://github.com/kelseyhightower/confd/blob/v0.16.0/docs/templates.md). The entrypoint
runs the configuration for each template once at startup of the container.

Templates must be written as `*.tmpl` files and placed below `/etc/confd/templates`. For each template a configuration
file written as `*.toml` must be placed below `/etc/confd/conf.d`.

Confd allows the use of different backends to provide the template key/value pairs. By default the backend `env` will be
used but it can be configured using the environment variable `CONFD_BACKEND`. A key prefix can be provided to separate
the configuration for different deployment environments or scenarios. The prefix can be defined using the environment
variable `CONFD_PREFIX`. Using `CONFD_ARGS` arbitrary command-line args can be passed to confd. A complete list of
args, can be found [here](https://github.com/kelseyhightower/confd/blob/v0.16.0/docs/command-line-flags.md).


## How to base your CoreMedia application on this image

Below you see an example how to create a docker file for a Spring-Boot application image based on this image:

#### Example Dockerfile:
```
ARG JAVA_APPLICATION_BASE_IMAGE_TAG
FROM coremedia/java-application-base:${JAVA_APPLICATION_BASE_IMAGE_TAG}
EXPOSE 8080 8199
COPY --chown=coremedia:coremedia src/docker /coremedia
COPY --chown=coremedia:coremedia target/myApp.jar /coremedia/application.jar
CMD ["application"]
```

## How to build this image

For each JVM base image, there is one Dockerfile:

* `Dockerfile.corretto`
* `Dockerfile.openjdk`

To build the images just use the `docker build` command in conjunction with the `-f` paramter for the Dockerfile:

```
docker build --tag myjava-application-base:latest -f Dockerfile.corretto .
```

You can also use the `Makefile` with one of the targets:
* `build`
* `tag`
* `push`
* `clean`
* `changelog`
* `<DIST>`
* `build-<DIST>`
* `tag-<DIST>`
* `push-<DIST>`

where `<DIST>` can be one of:
* `corretto`
* `openjdk-jre`
* `openjdk-jdk`

```
make corretto
```

During the make build, `git describe` will be used to determine the current build version.
Concrete, git describe will be called with the follwing parameters:

```
git describe --abbrev=4 --dirty=-SNAPSHOT
```

For a developer this means, your images will be tagged like this:

* on an annotated release tag, i.e. `2.2.0` the image tag will be `2.2.0`
* on an uncommitted changes on `2.2.0`, your image tag will be `2.2.0-SNAPSHOT`
* on a commit with hash `fsd23sdsa`, 3 commits after the above annotated tag `2.2.0`, your image tag will be `2.2.0-3-gfsd23`
* on an uncommitted change on `fsd23sdsa`, your image tag will be `2.2.0-3-gfsd23-SNAPSHOT`

The repository prefix for all images can be set using `REPOSITORY_PREFIX` environment variable:

```
make -e REPOSITORY_PREFIX=FOO corretto
```

To update the `CHANGELOG.md` run `make changelog`. For the changelog generation we use git-chglog, which reacts
on certain commit messages. Use the following keywords to start your commit message headline, the commits will be grouped.

* `feat:` - New Features
* `fix:` - Bug Fixes
* `updates:` - Thirdparty Updates
* `perf:` - Performance Optimizations
* `refactor:` - Refactorings

## Release
To release, tag the repo using this command:

```
git tag -a -m "release" MAJOR.MINOR.PATCH
```

## Jenkinsfile

This project contains a Jenkinsfile, we need to build this image in our internal CI.

License
=======

As with all Docker images, this image also contain other software which may be under other licenses (such as Bash, coreutils, etc
from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

* [prometheus jmx exporter](https://github.com/prometheus/jmx_exporter) - [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
* [confd](https://github.com/kelseyhightower/confd) - [MIT Licence](https://github.com/kelseyhightower/confd/blob/master/LICENSE)
* [docker-compose-wait](https://github.com/ufoscout/docker-compose-wait) - [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant
licenses for all software contained within.


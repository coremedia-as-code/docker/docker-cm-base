#!/bin/sh
set -e
if [ "${DEBUG_ENTRYPOINT}" = "true" ]; then
  echo "[DOCKER ENTRYPOINT] - DEBUG_ENTRYPOINT detected, all commands will be printed"
  WAIT_LOGGER_LEVEL=debug
  set -x
fi
echo "[DOCKER ENTRYPOINT] - starting entrypoint chain: $@"
if [ -n "${WAIT_HOSTS}" ] || [ -n "${WAIT_PATHS}" ]; then
  echo "[DOCKER ENTRYPOINT] - waiting for service dependencies..."
  /usr/bin/docker-compose-wait && exec "./${@:-application}"
else
  exec "./${@:-application}"
fi

# wait for 30 seconds after finishing entrypoint scripts to prevent any service from starting a restart loop
# on startup failures, which would result in unneccessary high load
echo "[DOCKER ENTRYPOINT] - waiting for 30 seconds before killing the container"
sleep 30
